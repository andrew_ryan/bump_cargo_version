#[allow(warnings)]
pub mod cli {
    pub fn cli() {
        run("./Cargo.toml");
    }
    fn get_cargo_toml_string(path: &str) -> String {
        std::fs::read_to_string(path)
            .expect("Please make sure that the current folder Cargo.toml file exists")
    }

    fn bump_version(version: String) -> String {
        let version_string = version.to_string();
        let mut version_vec = doe::split_to_vec!(version_string, ".");
        let mut version_vec_last = version_vec.last().unwrap().parse::<usize>().unwrap();
        version_vec_last += 1;
        let new_version = if version_vec.len() == 1 {
            format!("{}", version_vec_last)
        } else {
            version_vec.remove(version_vec.len() - 1);
            format!("{}.{}", version_vec.clone().join("."), version_vec_last)
        };
        new_version
    }
    fn run(path: &str) {
        let toml_string = get_cargo_toml_string(path);
        let cargo_toml = tsu::toml_from_str(toml_string.clone());
        let package = cargo_toml
            .get("package")
            .expect("Cargo.toml don't find the package table");
        let version = package
            .get("version")
            .expect("package table don't find the version key");
        let name = package
            .get("name")
            .expect("package table don't find the name key");
        let name_string = name.as_str().unwrap().to_string();
        let version_string = version.as_str().unwrap().to_string();
        let mut version_vec = doe::split_to_vec!(version_string, ".");
        let mut version_vec_last = version_vec.last().unwrap().parse::<usize>().unwrap();
        version_vec_last += 1;
        let new_version = if version_vec.len() == 1 {
            format!("{}", version_vec_last)
        } else {
            version_vec.remove(version_vec.len() - 1);
            format!("{}.{}", version_vec.clone().join("."), version_vec_last)
        };
        println!(
            "Upgrading {} from {} to {}",
            name_string, version_string, new_version
        );
        let mut toml_string_split = toml_string.split("\n").map(|s|s.to_string()).collect::<Vec<String>>();
        let find_index = |toml_string_split: Vec<String>,old_version:String| {
            let mut index: usize = 0;
            for (i, s) in toml_string_split.iter().enumerate() {
                if s.contains("version")&&s.contains(&old_version)&&!s.contains("}")&&!s.contains("{") {
                    index = i;
                    break;
                }
            }
            index
        };
        let find_package_index = |toml_string_split: Vec<String>| {
            let mut index: usize = 0;
            for (i, s) in toml_string_split.iter().enumerate() {
                if s.contains("[package]") {
                    index = i;
                    break;
                }
            }
            index
        };
        let package_index = find_package_index(toml_string_split.clone());
        let package_to_last = toml_string_split.get(package_index..).unwrap().to_vec();
        let version_index = find_index(package_to_last.clone(),version_string);
        toml_string_split[package_index+version_index] = format!("version = \"{}\"", new_version);
        let toml_string_join = toml_string_split.join("\n");
        std::fs::write(path, toml_string_join).unwrap();
    }
}
