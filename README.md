# Command line tool for bump up cargo version
[![Crates.io](https://img.shields.io/crates/v/fus.svg)](https://crates.io/crates/bump_cargo_version)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/bump_cargo_version)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/bump_cargo_version/-/raw/master/LICENSE)

![bump.gif](https://gitlab.com/andrew_ryan/bump_cargo_version/-/raw/master/bump.gif)

```sh
Command line tool for bump up cargo version
Example:
[package]
name = "duma"
version = "2.7.27"
edition = "2021"

> bump_cargo_version
>Upgrading duma from 2.7.27 to 2.7.28

```
